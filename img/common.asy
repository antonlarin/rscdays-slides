unitsize(0.5cm);

// palette
pen unnblue1 = rgb(0.333, 0.576, 0.761);
pen unnblue2 = rgb(0.212, 0.486, 0.694);
pen unnblue3 = rgb(0.059, 0.404, 0.667);
pen unnblue4 = rgb(0.031, 0.31, 0.525);
pen unnblue5 = rgb(0.02, 0.243, 0.412);

pen orange1 = rgb(1.0, 0.745, 0.4);
pen orange2 = rgb(1.0, 0.682, 0.255);
pen orange3 = rgb(1.0, 0.58, 0.024);
pen orange4 = rgb(0.816, 0.467, .0);
pen orange5 = rgb(0.639, 0.365, .0);

pen green1 = rgb(0.314, 0.788, 0.584);
pen green2 = rgb(0.169, 0.729, 0.49);
pen green3 = rgb(0.016, 0.706, 0.412);
pen green4 = rgb(0.0, 0.557, 0.318);
pen green5 = rgb(0.0, 0.439, 0.251);

